const puppeteer = require('puppeteer-extra');
const PORT = 3002;
const express = require('express');
const app = express();
const solve = require('./index.js');
const pluginStealth = require('puppeteer-extra-plugin-stealth');
const qs = require('qs');
app.use(express.json());
const axios2 = require('axios');
puppeteer.use(pluginStealth());
app.get('/nodejsapp/', (req, res) => {
    res.end("aas");
});
app.post('/google', (req, res) => {
    var reqBody = req.body['data'];

    puppeteer.launch({
        args: ["--proxy-server='direct://'", '--proxy-bypass-list=*', '--no-sandbox', '--disable-dev-shm-usage']
    }).then(async browser => {

        let elementTxtArr2 = {};
        for (var i = 0; i < reqBody.length; i++) {
            var page = await browser.newPage();
            await page.goto('https://www.google.com/search?q=' + "\"" + reqBody[i] + "\"", {waitUntil: 'domcontentloaded'});
            let recaptcha = await page.evaluate(() => {
                let el = document.querySelector("#recaptcha")
                return el ? true : false
            });
            if (recaptcha) {
                await solve(page);
            }
            const result = await page.evaluate(async (b) => {

                function escapeHtml(text) {
                    var map = {
                        "'": '’'
                    };

                    return text.replace(/[&<>"']/g, function (m) {
                        return map[m];
                    });
                }

                function stristr(haystack, needle, bool) {
                    let pos = 0
                    haystack += ''
                    pos = haystack.toLowerCase()
                        .indexOf((needle + '')
                            .toLowerCase())
                    if (pos === -1) {
                        return false
                    } else {
                        return true;
                    }
                }

                let elementTxtArr = {};
                var not_real = 0;
                document.querySelectorAll(".aCOpRe").forEach((a) => {

                    if (stristr(escapeHtml(a.innerText), b)) {
                        not_real = 1;
                    }
                    elementTxtArr[b] = not_real;
                });

                return elementTxtArr;
            }, reqBody[i]);
            console.log(reqBody.length + "-" + i);
            elementTxtArr2[reqBody[i]] = result[reqBody[i]];

        }
        if (typeof req.body['id'] === "undefined") {
            res.end(JSON.stringify(elementTxtArr2));
        } else {
            axios2.post('https://www.example.net', qs.stringify({
                'id': req.body['id'],
                'data': elementTxtArr2,
                'total_sentence': req.body['total_sentence']
            }))
                .then((res) => {
                    console.log(`statusCode: ${res.statusCode}`)
                    console.log(res)
                    console.log(`statusCode: ${res.data}`)
                })
                .catch((error) => {
                    console.error(error)
                })
        }

        await browser.close();

    });
    if (typeof req.body['id'] !== "undefined") {
        res.end(1);
    }
});

app.listen(PORT, () => console.log(`Started server. PORT: ` + PORT));